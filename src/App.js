import React, { Component, Suspense } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./scss/style.scss";

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

// Containers
const DefaultLayout = React.lazy(() => import("./layout/DefaultLayout"));
const DefaultLayoutAMA = React.lazy(() => import("./layout/DefaultLayoutAMA"));
const DefaultLayoutAdmin = React.lazy(() =>
  import("./layout/DefaultLayoutAdmin")
);

// Pages
const Login = React.lazy(() => import("./views/pages/login/Login"));
const LoginAMA = React.lazy(() => import("./views/pages/login/LoginAMA"));
const LoginAdmin = React.lazy(() => import("./views/pages/login/LoginAdmin"));
const Register = React.lazy(() => import("./views/pages/register/Register"));

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Suspense fallback={loading}>
          <Routes>
            <Route exact path="/login" name="Login Page" element={<Login />} />
            <Route
              exact
              path="/admin/login"
              name="Login Admin"
              element={<LoginAdmin />}
            />
            <Route
              exact
              path="/ama/login"
              name="Login AMA"
              element={<LoginAMA />}
            />
            <Route
              exact
              path="/register"
              name="Register Page"
              element={<Register />}
            />
            <Route
              path="/admin/*"
              name="Home"
              element={<DefaultLayoutAdmin />}
            />
            <Route path="/ama/*" name="Home" element={<DefaultLayoutAMA />} />
            <Route path="*" name="Home" element={<DefaultLayout />} />
          </Routes>
        </Suspense>
      </BrowserRouter>
    );
  }
}

export default App;
