import React from "react";

const ComingSoon = React.lazy(() => import("./views/pages/ComingSoon"));
const Page404 = React.lazy(() => import("./views/pages/page404/Page404"));

//Innovator
const Dashboard = React.lazy(() => import("./views/dashboard/Dashboard"));

//ADMIN==========
const DashboardAdmin = React.lazy(() => import("./views/admin/Dashboard"));

//AMA============
const DashboardAMA = React.lazy(() => import("./views/ama/Dashboard"));

const routes = [
  { path: "/", name: "Home" },
  { path: "/dashboard", name: "Dashboard", element: Dashboard },
  { path: "/404", name: "404", element: Page404 },

  //EVENT
  { path: "/event", name: "Event" },
  { path: "/event/list", name: "Event List", element: ComingSoon },
  { path: "/event/new", name: "New Event", element: ComingSoon },
  {
    path: "/event/submission/new",
    name: "Submission",
    element: ComingSoon,
  },
  {
    path: "/event/submission/list",
    name: "Submission List",
    element: ComingSoon,
  },
  {
    path: "/event/appointmen/approval",
    name: "Appointment Approval",
    element: ComingSoon,
  },
  { path: "/event/appointment", name: "Appointment", element: ComingSoon },

  //INNOVATION
  { path: "/innovation", name: "Innovation" },
  { path: "/innovation/list", name: "Innovation List", element: ComingSoon },
  {
    path: "/innovation/detail",
    name: "Innovation Detail",
    element: ComingSoon,
  },

  //ANNOUNCEMENT
  { path: "/announcement", name: "Announcement" },
  {
    path: "/announcement/list",
    name: "Announcement List",
    element: ComingSoon,
  },
  {
    path: "/announcement/new",
    name: "Create Announcement",
    element: ComingSoon,
  },
];

const routesAdmin = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", element: DashboardAdmin },
  { path: "/404", name: "404", element: Page404 },
];

const routesAMA = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", element: DashboardAMA },
  { path: "/404", name: "404", element: Page404 },
];

export { routes, routesAdmin, routesAMA };
