import React from "react";
import { CFooter, CCol, CRow, CContainer } from "@coreui/react";
import logo from "src/assets/images/logo_white_big.png";
import logoAthon from "src/assets/images/athon.png";
import logoTelkom from "src/assets/images/telkom.png";

const AppFooter = () => {
  return (
    <CFooter className="footermokap text-white">
      <CContainer>
        <CRow>
          <CCol sm={12} lg={3}>
            <h3>About Us</h3>
            <p className="aboutus">
              Mokap (Monev Rekap), final project Telkom Athon of Team A. How
              Effective Your Company Handling Innovation?
            </p>
          </CCol>
          <CCol xs={12} sm={4} lg={3}>
            <img src={logo} className="img-center " />
          </CCol>
          <CCol xs={6} sm={4} lg={3}>
            <img src={logoAthon} className="img-center " />
          </CCol>
          <CCol xs={6} sm={4} lg={3}>
            <img src={logoTelkom} className="img-center " />
          </CCol>
        </CRow>
      </CContainer>
    </CFooter>
  );
};

export default React.memo(AppFooter);
