import React from "react";
import { Link, NavLink } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
  CContainer,
  CHeader,
  CHeaderBrand,
  CHeaderDivider,
  CHeaderNav,
  CHeaderToggler,
  CNavLink,
  CNavItem,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CDropdown,
  CFormInput,
  CInputGroup,
  CInputGroupText,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { cilBell, cilMagnifyingGlass, cilMenu } from "@coreui/icons";

import { AppBreadcrumb } from "./index";
import { AppHeaderDropdown } from "./header/index";
import { navigation } from "../_nav";
import logo from "src/assets/images/logo_white.png";

const AppHeader = () => {
  const dispatch = useDispatch();
  const sidebarShow = useSelector((state) => state.sidebarShow);

  return (
    <>
      <CHeader position="sticky" className="mb-4 bg-primary">
        <CContainer>
          <CHeaderToggler
            className="ps-1 d-md-none"
            onClick={() => dispatch({ type: "set", sidebarShow: !sidebarShow })}
          >
            <CIcon icon={cilMenu} size="lg" />
          </CHeaderToggler>
          <CNavLink
            to="/dashboard"
            className="d-none d-md-block"
            component={NavLink}
          >
            <img src={logo} />
          </CNavLink>
          <CHeaderBrand className="mx-auto d-md-none text-white" to="/">
            <CNavLink to="/dashboard" component={NavLink}>
              <img src={logo} />
            </CNavLink>
          </CHeaderBrand>
          <CHeaderNav className="d-none d-md-flex me-auto">
            {navigation.map(function (item) {
              if (item.items == null) {
                return (
                  <CNavItem className="header-link">
                    <CNavLink to={item.to} component={NavLink}>
                      {item.name}
                    </CNavLink>
                  </CNavItem>
                );
              } else {
                //untuk dropdown
                return (
                  <CDropdown variant="nav-item">
                    <CDropdownToggle color="secondary">
                      {item.name}
                    </CDropdownToggle>
                    <CDropdownMenu>
                      {item.items.map(function (subItem) {
                        return (
                          <CNavLink to={subItem.to} component={NavLink}>
                            <CDropdownItem>{subItem.name}</CDropdownItem>
                          </CNavLink>
                        );
                      })}
                    </CDropdownMenu>
                  </CDropdown>
                );
              }
            })}
          </CHeaderNav>
          <CHeaderNav className="d-none d-lg-block">
            <CInputGroup className="input-prepend ">
              <CFormInput type="text" placeholder="Search" />
              <CInputGroupText>
                <CIcon icon={cilMagnifyingGlass} />
              </CInputGroupText>
            </CInputGroup>
          </CHeaderNav>
          <CHeaderNav>
            <CDropdown variant="nav-item">
              <CDropdownToggle caret={false}>
                <CIcon icon={cilBell} size="lg" />
              </CDropdownToggle>
              <CDropdownMenu>
                <CDropdownItem>Lengkapi data anda</CDropdownItem>
                <CHeaderDivider style={{ width: "100%" }} className="ms-0" />
                <CDropdownItem>
                  Selamat datang di Mokap (Monev Rekap) versi beta
                </CDropdownItem>
              </CDropdownMenu>
            </CDropdown>
          </CHeaderNav>
          <CHeaderNav className="ms-3">
            <AppHeaderDropdown />
          </CHeaderNav>
        </CContainer>
        {/* <CHeaderDivider /> */}
      </CHeader>
      <CContainer>
        <AppBreadcrumb />
      </CContainer>
      <br />
    </>
  );
};

export default AppHeader;
