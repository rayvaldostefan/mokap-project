import React from 'react'
import {
  CRow,
  CCol,
  CWidgetStatsA,
} from '@coreui/react'

const WidgetsDropdown = (props) => {
  return (
    <CRow>
      <CCol sm={6} lg={3}>
        <CWidgetStatsA
          className="mb-4"
          color="primary"
          value={
            <>
            Total Inovasi
            <p class="my-0" style={{fontSize:'40px'}}>{props.totInovasi}</p>
            <p style={{fontSize:'10px'}}>(naik {props.growth}% dari batch sebelumnya)</p>
            </>
          }
        />
      </CCol>
      <CCol sm={6} lg={3}>
        <CWidgetStatsA
          className="mb-4"
          color="info"
          value={
            <>
            Progres Inovasi
            <p class="my-0" style={{fontSize:'40px'}}>{props.progInovasi}%</p>
            <p style={{fontSize:'10px'}}>(Average nasional)</p>
            </>
          }
        />
      </CCol>
      <CCol sm={6} lg={3}>
        <CWidgetStatsA
          className="mb-4"
          color="danger"
          value={
            <>
            Total Pengeluaran
            <p class="my-3" style={{fontSize:'20px'}}>IDR {props.totPengeluaran}</p>
            <p style={{fontSize:'10px'}}>({props.szPengeluaran}% dari estimasi pengeluaran)</p>
            </>
          }
        />
      </CCol>
      <CCol sm={6} lg={3}>
        <CWidgetStatsA
          className="mb-4"
          color="warning"
          value={
            <>
            Total Pendapatan
            <p class="my-3" style={{fontSize:'20px'}}>IDR {props.totPendapatan}</p>
            <p style={{fontSize:'10px'}}>({props.szPendapatan}% dari estimasi pendapatan)</p>
            </>
          }
        />
      </CCol>
     
    </CRow>
  )
}

export default WidgetsDropdown
