import React from "react";
import { CCol, CContainer, CRow } from "@coreui/react";
import logo from "src/assets/images/logo.png";

const ComingSoon = () => {
  return (
    <div className="bg-light mt-4">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={6}>
            <div className="clearfix mt-4">
              <h1 className="float-start me-4">
                <img src={logo} style={{ width: "150px" }} />
              </h1>
              <h4 className="pt-2">COMING SOON</h4>
              <p className="text-medium-emphasis float-start">
                Under construction.
              </p>
            </div>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default ComingSoon;
