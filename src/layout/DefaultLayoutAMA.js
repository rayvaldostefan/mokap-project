import React from "react";
import {
  AppContent,
  AppSidebarAMA,
  AppFooter,
  AppHeaderAMA,
} from "../components/index";

// routes config
import { routesAMA } from "../routes";

const DefaultLayout = () => {
  return (
    <div>
      <AppSidebarAMA />
      <div className="wrapper d-flex flex-column min-vh-100 bg-light">
        <AppHeaderAMA />
        <div className="body flex-grow-1 px-3">
          <AppContent routes={routesAMA} />
        </div>
        <AppFooter />
      </div>
    </div>
  );
};

export default DefaultLayout;
