import React from "react";
import {
  AppContent,
  AppSidebarAdmin,
  AppFooter,
  AppHeaderAdmin,
} from "../components/index";

// routes config
import { routesAdmin } from "../routes";

const DefaultLayout = () => {
  return (
    <div>
      <AppSidebarAdmin />
      <div className="wrapper d-flex flex-column min-vh-100 bg-light">
        <AppHeaderAdmin />
        <div className="body flex-grow-1 px-3">
          <AppContent routes={routesAdmin} />
        </div>
        <AppFooter />
      </div>
    </div>
  );
};

export default DefaultLayout;
