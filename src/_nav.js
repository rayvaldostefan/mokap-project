import React from "react";
import CIcon from "@coreui/icons-react";
import {
  cilCursor,
  cilVoiceOverRecord,
  cilCalendarCheck,
  cilSpeedometer,
  cilStar,
} from "@coreui/icons";
import { CNavGroup, CNavItem, CNavTitle } from "@coreui/react";

const navigation = [
  {
    component: CNavItem,
    name: "Home",
    to: "/home",
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
  },
  {
    component: CNavGroup,
    name: "Event",
    to: "/event",
    icon: <CIcon icon={cilCursor} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: "Event List",
        to: "/event/list",
      },
      {
        component: CNavItem,
        name: "Create New Event",
        to: "/event/new",
      },
      {
        component: CNavItem,
        name: "Submission",
        to: "/event/submission/new",
      },
      {
        component: CNavItem,
        name: "List of Submission",
        to: "/event/submission/list",
      },
      {
        component: CNavItem,
        name: "Appointment Approval",
        to: "/event/appointmen/approval",
      },
      {
        component: CNavItem,
        name: "Appointment",
        to: "/event/appointment",
      },
    ],
  },
  {
    component: CNavGroup,
    name: "Innovation",
    to: "/innovation",
    icon: <CIcon icon={cilStar} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
    items: [
      {
        component: CNavItem,
        name: "Innovation List",
        to: "/innovation/list",
      },
      {
        component: CNavItem,
        name: "My Innovation",
        to: "/innovation/detail",
      },
    ],
  },

  {
    component: CNavGroup,
    name: "Announcement",
    to: "/announcement",
    icon: <CIcon icon={cilVoiceOverRecord} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: "Create Announcement",
        to: "/announcement/new",
      },
      {
        component: CNavItem,
        name: "Announcement",
        to: "/announcement/list",
      },
    ],
  },

  {
    component: CNavItem,
    name: "Dashboard",
    to: "/dashboard",
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
  },
];

const navigationAdmin = [
  {
    component: CNavItem,
    name: "Dashboard",
    to: "/admin/dashboard",
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
  },
  {
    component: CNavGroup,
    name: "Event",
    to: "/admin/event",
    icon: <CIcon icon={cilCursor} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: "Event List",
        to: "/admin/event/list",
      },
      {
        component: CNavItem,
        name: "Create New Event",
        to: "/admin/event/new",
      },
    ],
  },
  {
    component: CNavItem,
    name: "Innovation",
    to: "/admin/innovation",
    icon: <CIcon icon={cilStar} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
  },

  {
    component: CNavItem,
    name: "Appointment",
    to: "/admin/appointment",
    icon: <CIcon icon={cilCalendarCheck} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
  },

  {
    component: CNavItem,
    name: "Announcement",
    to: "/admin/announcement",
    icon: <CIcon icon={cilVoiceOverRecord} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
  },
];

const navigationAMA = [
  {
    component: CNavItem,
    name: "Dashboard",
    to: "/ama/dashboard",
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
  },
  {
    component: CNavGroup,
    name: "Event",
    to: "/ama/event",
    icon: <CIcon icon={cilCursor} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: "Event List",
        to: "/ama/event/list",
      },
      {
        component: CNavItem,
        name: "Create New Event",
        to: "/ama/event/new",
      },
    ],
  },
  {
    component: CNavItem,
    name: "Innovation",
    to: "/ama/innovation",
    icon: <CIcon icon={cilStar} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
  },

  {
    component: CNavItem,
    name: "Appointment",
    to: "/ama/appointment",
    icon: <CIcon icon={cilCalendarCheck} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
  },

  {
    component: CNavItem,
    name: "Announcement",
    to: "/ama/announcement",
    icon: <CIcon icon={cilVoiceOverRecord} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
  },
];
export { navigation, navigationAMA, navigationAdmin };
