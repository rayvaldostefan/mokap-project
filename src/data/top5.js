const top5 = [
    {
        id:'1',
        name:'Team A',
        progress: 85,
    }, 
    {
        id:'3',
        name:'Team B',
        progress: 80,
    }, 
    {
        id:'5',
        name:'Team C',
        progress: 82.5,
    }, 
    {
        id:'9',
        name:'Team D',
        progress: 75,
    }, 
    {
        id:'11',
        name:'Team E',
        progress: 65.3,
    }, 
    ];
 
  
  export default top5;